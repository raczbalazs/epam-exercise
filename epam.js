let selenium = require('selenium-webdriver');
let test = require('selenium-webdriver/testing');
let assert = require('assert');
let screenShotCounter = 0;
let browser;

test.describe('Testing the EPAM careers page', function() {
    test.beforeEach(function(done) {
        this.timeout(20000);        
        browser = new selenium.Builder().forBrowser('chrome').build();        
        browser.manage().timeouts().pageLoadTimeout(20000);
        browser.manage().window().maximize();
        browser.get('https://www.epam.com/careers');
        screenShotCounter++;

        done();
    });

    test.afterEach(function(done) {
        browser.takeScreenshot().then(
            function(image, err) {
                require('fs').writeFile('out'+screenShotCounter+'.png', image, 'base64', function(err) {
                    if(err) 
                        console.log(err);
                });
            }
        );
        browser.quit();

        done();
    })

    test.it('The given link will take me to the correct site', function(done) {
        browser.getTitle().then(currentTitle => {
            assert.equal(currentTitle, 'Explore Professional Growth Opportunities| EPAM Careers');
            done();
        })
    })

    test.it('Clicking on the EPAM logo takes me to the Home Page', function(done) {
        browser.findElement(selenium.By.className('header__logo')).click();
        browser.getTitle().then(currentTitle => {            
            assert.equal(currentTitle, 'EPAM | Software Engineering & Product Development Services');
            done();
        })
    })

    test.it('Clicking on the "Home" link takes me to the Home Page', function(done) {
        browser.findElement(selenium.By.linkText('Home')).click();
        browser.getTitle().then(currentTitle => {            
            assert.equal(currentTitle, 'EPAM | Software Engineering & Product Development Services');
            done();
        })
    })
    test.it('Clicking on "Engineer" link inside "What we do" takes me to the Engineer page', function(done) {
        browser.findElement(selenium.By.linkText('WHAT WE DO')).then(element => {
            browser.actions().mouseMove(element).perform();
            browser.findElement(selenium.By.linkText('ENGINEER')).click()
            browser.getCurrentUrl().then(currentUrl => {            
                assert.equal(currentUrl, 'https://www.epam.com/what-we-do/engineer');
                done();
            })   
        })  
    })

    test.it('Clicking the Accept in the Cookie disclaimer makes the disclaimer dissapear', function(done) {
        let cookieButton = browser.findElement(selenium.By.css('.cookie-disclaimer__column > button'))
        cookieButton.click();
        browser.wait(selenium.until.stalenessOf(cookieButton)).then((condition) => {
            assert.equal(condition, true);
            done();
        })
    })

    test.it('Cookie disclaimer should not be visible on any page, after accepting them', function(done) {
        let cookieButton = browser.findElement(selenium.By.css('.cookie-disclaimer__column > button'))
        cookieButton.click();
        browser.findElement(selenium.By.className('recruiting-search__submit')).click()
        browser.wait(selenium.until.stalenessOf(cookieButton)).then((condition) => {
            assert.equal(condition, true);
            done();
        })
    })

    test.it('Clicking on the link in the Cookie Disclaimer takes me to the Cookie Policy page', function(done) {
        browser.findElement(selenium.By.css('.cookie-disclaimer__description > p > a')).click();
        browser.getTitle().then(currentTitle => {
            assert.equal(currentTitle, 'Cookie Policy');
            done();
        })
    })

    test.it('Checking the default parameter values', function(done) {
        browser.findElement(selenium.By.id('new_form_job_search_1445745853_copy-keyword')).getText().then(idText => {
            browser.findElement(selenium.By.className('select2-selection__rendered')).getText().then(locationText => {
                browser.findElement(selenium.By.className('default-label')).getText().then(skillText => {
                    assert.deepStrictEqual([idText, locationText, skillText], ['', 'Debrecen', 'All Skills'])
                    done();
                })
            })
        })
    })

    test.it('Clicking on the "Find" button takes me to the Job Listings page', function(done) {
        browser.findElement(selenium.By.className('recruiting-search__submit')).click()
        browser.getTitle().then(currentTitle => {
            assert.equal(currentTitle, 'Job Listings')
            done();
        })
    })

    test.it('Pressing enter in the id field should take me to the "Job Listings" page', function(done) {
        browser.findElement(selenium.By.css('input[id = new_form_job_search_1445745853_copy-keyword]')).sendKeys("\n")
        browser.getTitle().then(currentTitle => {
            assert.equal(currentTitle, 'Job Listings')
            done();
        })
    })

    test.it('Initiating the search without changing the parameters should list all available jobs in the default "Location"', function(done) {
        let myLocation;
        let allInMyLocation = true;
        browser.findElement(selenium.By.className('select2-selection__rendered')).getAttribute('title').then(title => {
            myLocation = title;
        })
        browser.findElement(selenium.By.className('recruiting-search__submit')).click()
        browser.findElements(selenium.By.className('search-result__location')).then(locations => {
            locations.forEach(location => {
                location.getText().then(locationName => {
                    if(!locationName.includes(myLocation)) {
                        allInMyLocation = false;
                    }
                })
            });
            assert.equal(allInMyLocation, true)
            done();
        })
    })

    test.it('The result list after the search should be as long as the page says', function(done) {
        browser.findElement(selenium.By.className('recruiting-search__submit')).click();
        browser.findElement(selenium.By.className('search-result__heading')).getText().then(text => {
            browser.findElements(selenium.By.css('div[class = search-result__list] > article')).then(result => {
                assert.equal(result.length, text.match(/\d+/)[0]);
                done();
            })
        })
    })

    test.it('Changing the parameters on the search page should list the right jobs', function(done) {
        let searchText = 'test'
        let names = []
        browser.findElement(selenium.By.id('new_form_job_search_1445745853_copy-keyword')).sendKeys(searchText + '\n')
        browser.findElements(selenium.By.className('search-result__item-name')).then(elements => {
            elements.forEach(element => {
                element.getText().then(elementName => {
                    names.push(elementName);
                });
            })
            let correctCounter = 0;
            names.forEach(name => {
                if (name.includes(searchText))
                    correctCounter++
            })
            assert.equal(correctCounter, names.length)
            done();
        })
    })

    test.it('The location field should auto-complete from it\'s list', function(done) {
        let locationText = 'debre'
        browser.findElement(selenium.By.className('select2-selection__rendered')).click();
        browser.findElement(selenium.By.className('select2-search__field')).sendKeys(locationText)
            browser.findElement(selenium.By.id('select2-new_form_job_search_1445745853_copy-location-results')).then(element => {
                element.isDisplayed().then(result => {
                    assert.equal(result, true)
                    done();
                })
            })
    })

    test.it('Navigating back to the "Search Page" after a search should reset the ID parameter', function(done) {
        let text = 'blabla';
        browser.findElement(selenium.By.id('new_form_job_search_1445745853_copy-keyword')).sendKeys(text + '\n')
        browser.navigate().back();
        browser.findElement(selenium.By.id('new_form_job_search_1445745853_copy-keyword')).getText().then(currentText => {
            assert.equal(text!==currentText, true)
            done();
        })
    })
    //ugly
    test.it('After choosing skills the page should show below which skills have been chosen', function(done) {
        let clickedElements = 0;
        browser.findElement(selenium.By.className('multi-select-filter validation-focus-target job-search__departments')).click();
        browser.findElements(selenium.By.css('ul[class = multi-select-column] > li > label')).then(elements => {            
            for (let i=0;i<5;i++) {
                elements[i].click();
                clickedElements++;
            }
        }).then(() => {
            browser.findElement(selenium.By.className('multi-select-filter validation-focus-target job-search__departments')).click().then( () => {
                browser.findElements(selenium.By.css('ul[class = selected-items] > li')).then(elements => {
                    assert.equal(elements.length, clickedElements);
                    done();
                })
            })
        })
    })

    test.it('I should be able to apply to the found jobs', function(done) {
        browser.findElement(selenium.By.className('recruiting-search__submit')).click();
        browser.findElement(selenium.By.xpath('//*[@id="main"]/div[1]/div[1]/section/div/div[1]/div/section/div[1]/article[1]/footer/div/a')).click();
        browser.findElement(selenium.By.className('form-constructor section')).then(element => {
            element.isEnabled().then(result=> {
                assert.equal(result, true)
                done();
            })
        })
    })
    //there should be a greater than assertation
    test.it('Should be able to share the jobs', function(done) {
        browser.findElement(selenium.By.className('recruiting-search__submit')).click();
        browser.findElement(selenium.By.xpath('//*[@id="main"]/div[1]/div[1]/section/div/div[1]/div/section/div[1]/article[1]/footer/div/a')).click();
        browser.findElement(selenium.By.xpath('//*[@id="main"]/article/div/section/div[1]/div/div/ul[1]/li[2]/a')).click().then(() => {
            browser.getAllWindowHandles().then(handles => {
                assert.equal(handles.length, 2);
                done();
            })
        })
    })
    
    test.it('Not finding jobs with given parameter should have a message displayed', function(done) {
        browser.findElement(selenium.By.id('new_form_job_search_1445745853_copy-keyword')).sendKeys("Not existing job ID\n");
        browser.findElement(selenium.By.className('search-result__error-message')).getText().then(text => {
            assert.equal(text, 'Sorry, your search returned no results. Please try another combination.');
            done();
        })
    })

    test.it('Checking relocation help on the result page should only show jobs that have relocation help assigned to them', function(done) {
        let resultCount = 0;
        let logoCount = 0;
        let locationField;
        browser.findElement(selenium.By.className('select2-selection__rendered')).click();
        browser.findElement(selenium.By.className('select2-search__field')).sendKeys('Amsterdam\n')
        browser.findElement(selenium.By.className('recruiting-search__submit')).click();
        browser.findElement(selenium.By.xpath('//*[@id="main"]/div[1]/div[1]/section/div/div[1]/div/form/fieldset/p/label')).then(element => {
            element.click();
            browser.findElements(selenium.By.css('div[class = search-result__list] > article')).then(elements => {
                resultCount = elements.length;
                browser.findElements(selenium.By.className('search-result__item-type search-result__item-type--relocation')).then(elements => {
                    logoCount = elements.length;
                    assert.equal(logoCount, resultCount);
                    done();
                })
            })
        })
    })

    test.it('Special characters such as ascii, emoji ciril letters etc should not break the application', function(done) {
        browser.findElement(selenium.By.id('new_form_job_search_1445745853_copy-keyword')).sendKeys("$ß#{ѪѬѦѨ 😁\n");
        browser.getTitle().then(currentTitle => {            
            assert.equal(currentTitle, 'Job Listings')
            done();
        })
    })

    test.it('SQL injections should not be possible', function(done) {
        browser.findElement(selenium.By.id('new_form_job_search_1445745853_copy-keyword')).sendKeys("test’ or 1=1– \n");
        browser.getTitle().then(currentTitle => {            
            assert.equal(currentTitle, 'Job Listings')
            done();
        })
    })
    //needs to scroll down to work with random numbers
    test.it('Selecting a job should take me to it\'s page', function(done) {
        let randomNumber;
        browser.findElement(selenium.By.id('new_form_job_search_1445745853_copy-keyword')).sendKeys("\n")
        browser.findElements(selenium.By.css('.search-result__list > article')).then(elements=> {
            randomNumber = Math.floor(Math.random()*elements.length)
            let element = elements[0];
            element.findElement(selenium.By.className('search-result__item-name')).then(name=> {
                name.getText().then(selectedName => {
                    name.click();
                    browser.findElement(selenium.By.css('.recruiting-page__header > h1')).getText().then(actualName => {

                        assert.equal(actualName, selectedName);
                        done();
                    })
                })
            })
        })
    })

}); 